package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.MaxTimePage;

import net.thucydides.core.annotations.Step;

public class MaxTimeSteps {
	
	MaxTimePage maxTimePage;
	
	@Step
	public void login(String strUsuario,String strContraseña) {
		maxTimePage.open();
		maxTimePage.login(strUsuario, strContraseña);
	}
	
	@Step
	public void seleccionarFecha() {
		maxTimePage.seleccionarFecha();		
	}
	
	@Step
	public void seleccionarNuevo() {
		maxTimePage.seleccionarNuevo();
		
	}
	
	@Step
	public void diligenciarCampos(List<List<String>> data, int id) throws InterruptedException {
		maxTimePage.seleccionarProyecto();
		maxTimePage.diligenciarHoras();
		maxTimePage.diligenciarComentario();
		
	}

}
