package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.MaxTimeSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class MaxTimeDefinition {
	
	@Steps
	MaxTimeSteps maxTimeSteps;
	
	@Given("^Me authentico con usuario \"([^\"]*)\" y pass \"([^\"]*)\"$")
	public void me_authentico_con_usuario_y_pass(String strUsuario, String strContraseña)  {
	    maxTimeSteps.login(strUsuario, strContraseña);
	}

	@Given("^Selecciono el dia \"([^\"]*)\"$")
	public void selecciono_el_dia(String arg1)  {
	   maxTimeSteps.seleccionarFecha();
	}

	@Given("^selecciono Nuevo Reporte$")
	public void selecciono_Nuevo_Reporte()  {
	    maxTimeSteps.seleccionarNuevo();
	}

	@When("^Diligencio el detalle$")
	public void diligencio_el_detalle(DataTable dtDatosForms) throws InterruptedException  {
		List<List<String>> data = dtDatosForms.raw();
		maxTimeSteps.diligenciarCampos(data,1);
	}

	@When("^busco Servicio \"([^\"]*)\"$")
	public void busco_Servicio(String arg1)  {
	    
	}

	@Then("^Verifico Cierre Exito$")
	public void verifico_Cierre_Exito()  {
	    
	}

}
