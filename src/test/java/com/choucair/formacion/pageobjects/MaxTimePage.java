package com.choucair.formacion.pageobjects;



import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;

@DefaultUrl("http://190.90.59.4:18000/MaxtimeCHC/Login.aspx?ReturnUrl=%2fMaxtimeCHC%2fdefault.aspx")
public class MaxTimePage extends PageObject{
	
	//campo usuario
	@FindBy(xpath="//*[@id=\'Logon_v0_MainLayoutEdit_xaf_l30_xaf_dviUserName_Edit_I\']")
	private WebElementFacade usuario;
	
	//campo password
	@FindBy(xpath="//*[@id=\'Logon_v0_MainLayoutEdit_xaf_l35_xaf_dviPassword_Edit_I\']")
	private WebElementFacade contraseña;
	
	//bonton Login
	@FindBy(xpath="//*[@id=\'Logon_PopupActions_Menu_DXI0_T\']")
	private WebElementFacade btnLogin;
	
	//fecha
	@FindBy(xpath="//*[@id=\'Vertical_v1_LE_v2_tccell0_0\']")
	private WebElementFacade fecha;
	
	//boton nuevo
	@FindBy(xpath="//*[@id=\'Vertical_v3_MainLayoutView_xaf_l103_xaf_dviReporteDetallado_ToolBar_Menu_DXI0_T\']/a") 
	private WebElementFacade btnNuevo;
	
	//boton seleccionar proyecto
	@FindBy(xpath="//*[@id=\'Vertical_v15_MainLayoutEdit_xaf_l135_xaf_dviProyecto_Edit_Find_B\']")        
	private WebElementFacade btnSeleccionarProyecto;
	
	//lista proyecto
	@FindBy(xpath="//*[@id=\'Dialog_v9_LE_v10_DXDataRow0\']/td")	              
	private WebElementFacade lisProyecto;
	
	//texto horas
	@FindBy(xpath="//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l182_xaf_dviHoras_Edit_I\']")
	private WebElementFacade txtHoras;
	
	//texto comentario
	@FindBy(xpath="//*[@id=\'Vertical_v6_MainLayoutEdit_xaf_l207_xaf_dviComentario_Edit_I\']")
	private WebElementFacade txtComentario;
	
	public void login(String strUsuario, String strContraseña) {
		usuario.click();
		usuario.clear();
		usuario.sendKeys(strUsuario);
		
		contraseña.click();
		contraseña.clear();
		contraseña.sendKeys(strContraseña);
		
		btnLogin.click();
	}
	
	public void seleccionarFecha() {
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fecha.click();
	}
	
	public void seleccionarNuevo() {
		btnNuevo.click();
	}
	
	public void seleccionarProyecto() throws InterruptedException {
		Thread.sleep(10000);
		btnSeleccionarProyecto.click();
		Thread.sleep(10000);
		lisProyecto.click();
		
	}

	public void diligenciarHoras() {
		txtHoras.sendKeys("8");		
	}
	
	public void diligenciarComentario() {
		txtComentario.sendKeys("Conexion Directa");
	}

}
